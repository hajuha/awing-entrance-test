
### Install [Node.js](https://nodejs.org/en/) v16


or <https://github.com/nvm-sh/nvm>:

```bash
nvm install 16 && nvm use 16
```

### Run the development server

```bash
yarn dev
```

## Tech Stack

- [ReactJS v18](https://reactjs.org/docs/hello-world.html)
- [Material UI v5](https://mui.com/material-ui/getting-started/overview/)
- [TypeScript](https://www.typescriptlang.org/)
