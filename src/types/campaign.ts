export type tCampaign = {
  information: tInformation;
  subCampaigns: tSubCampaign[];
};

export type ValueOf<T> = T[keyof T];

export type tInformation = {
  name: string;
  describe?: string;
};

export type tSubCampaign = {
  id: string;
  name: string;
  status: tCampaignStatusValue;
  ads: tAds[];
};

export type tAds = {
  id: string;
  name: string;
  quantity: number;
};

export const CAMPAIGN_STATUS = {
  inactive: false,
  active: true,
} as const;

export type tCampaignStatus = keyof typeof CAMPAIGN_STATUS;
export type tCampaignStatusValue = ValueOf<typeof CAMPAIGN_STATUS>;
