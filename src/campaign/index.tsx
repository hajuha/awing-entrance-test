import { Box, Button, Grid, Paper, Tab, Tabs } from '@mui/material';
import '../App.css';
import { useState } from 'react';
import { CustomTabPanel } from './components/custom-tabpanel';
import Infor from './components/information';
import SubCampaigns from './components/sub-campaigns';
import {
    tCampaign,
    CAMPAIGN_STATUS,
    tInformation,
} from '../types/campaign';
import * as _ from 'lodash';
import { v1 as uuidv1 } from 'uuid';
type TabValue = 0 | 1;
const initialCampaign: tCampaign = {
    information: {
        name: '',
        describe: '',
    },
    subCampaigns: [
        {
            id: uuidv1(),
            status: CAMPAIGN_STATUS.active,
            name: 'Chiến dịch con 1',

            ads: [
                {
                    id: uuidv1(),
                    name: 'Quảng cáo 1',
                    quantity: 1,
                },
            ],
        },
    ],
};

function CampaignPage() {
    const [selectedTab, setSelectedTab] = useState<TabValue>(0);
    const [information, setInformation] = useState(initialCampaign.information);
    const [submitAttempted, setSubmitAttempted] = useState(false);
    const [subCampaigns, setSubCampaigns] = useState(
        initialCampaign.subCampaigns
    );

    

    const handleChangeInformation = (field: string, value: string) => {
        setInformation((prev: tInformation) => ({
            ...prev,
            [field]: value,
        }));
    };

    const handleTabChange = (newValue: TabValue) => {
        setSelectedTab(newValue);
    };

    const validateData = () => {
        let isValid = true;
        if (!information.name) {
            isValid = false;
        }

        for (const subCampaign of subCampaigns) {
            if (!subCampaign.name) {
                isValid = false;
            }

            for (const ad of subCampaign.ads) {
                if (!ad.name || ad.quantity <= 0) {
                    isValid = false;
                }
            }
        }
        return isValid;
    };

    const submitData = () => {
        const data = {
            information,
            subCampaigns,
        };
        const isValid = validateData();


        if (isValid) {
            alert('Thành công !!! ' + JSON.stringify(data));
        } else alert('Có lỗi xảy ra, vui lòng kiểm tra thông tin đã nhập!');
        setSubmitAttempted(true);
    };

    return (
        <Paper>
            <Grid container xs={12} justifyContent='flex-end'>
                <Box
                    sx={{
                        isplay: 'flex',
                        padding: '10px 20px',
                        justifyContent: 'flex-end',
                    }}>
                    <Button onClick={() => submitData()} variant='contained'>
                        Submit
                    </Button>
                </Box>
            </Grid>

            <Tabs
                value={selectedTab}
                onChange={(_, value) => handleTabChange(value)}
                aria-label='Thông tin'>
                <Tab key={0} label={'Thông tin chiến dịch'} />

                <Tab key={1} label={'Chiến dịch con'} />
            </Tabs>

            <CustomTabPanel key={0} value={selectedTab} index={0}>
                <Infor
                    information={information}
                    submitAttempted={submitAttempted}
                    onChangeCampaignInfo={handleChangeInformation}
                />
            </CustomTabPanel>

            <CustomTabPanel key={1} value={selectedTab} index={1}>
                <SubCampaigns
                    subCampaigns={subCampaigns}
                    setSubCampaigns={setSubCampaigns}
                    submitAttempted={submitAttempted}
                />
            </CustomTabPanel>
        </Paper>
    );
}

export default CampaignPage;
