import {
    TableContainer,
    Paper,
    Table,
    TableCell,
    TableHead,
    TableRow,
    Checkbox,
    FormControlLabel,
    Button,
    TableBody,
    IconButton,
    TextField,
} from '@mui/material';
import { tAds, tSubCampaign } from '../../types/campaign';
import React, { useEffect, useState } from 'react';
import { v1 as uuidv1 } from 'uuid';
import { DeleteOutline } from '@mui/icons-material';
interface AdsProps {
    subCampaign: tSubCampaign;
    subCampaigns: tSubCampaign[];
    setSubCampaigns: (data: tSubCampaign[]) => void;
    submitAttempted: boolean;
}

const Ads = ({
    subCampaign,
    subCampaigns,
    setSubCampaigns,
    submitAttempted
}: AdsProps) => {
    const [selectedAdsIndex, setSelectedAdsIndex] = useState<string[]>([]);
    const [ads, setAds] = useState<tAds[]>([]);

    useEffect(() => {
        setAds(subCampaign.ads);
    }, [subCampaign]);

    const handleSetSubCampaigns = (newAds: tAds[]) => {
        const newSubCampaign: tSubCampaign[] = subCampaigns.map(
            (campaign: tSubCampaign) =>
                campaign.id === subCampaign.id
                    ? { ...campaign, ads: newAds }
                    : campaign
        );

        setSubCampaigns(newSubCampaign);
    };

    const handleSelectAllAds = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            setSelectedAdsIndex(ads.map((ad) => ad.id));
        } else {
            setSelectedAdsIndex([]);
        }
    };

    const handleSelectAds = (
        event: React.ChangeEvent<HTMLInputElement>,
        id: string
    ) => {
        if (selectedAdsIndex.find((ad) => ad === id))
            setSelectedAdsIndex(selectedAdsIndex.filter((_id) => _id !== id));

        if (event.target.checked) {
            setSelectedAdsIndex([...new Set([...selectedAdsIndex, id])]);
        } else
            setSelectedAdsIndex(selectedAdsIndex.filter((_id) => _id !== id));
    };

    const handleDeleteSelectedAds = () => {
        const newAds = ads.filter(
            (ads: tAds) => !selectedAdsIndex.includes(ads.id)
        );
        handleSetSubCampaigns(newAds);
        setAds(newAds);
        setSelectedAdsIndex([]);
    };

    const handleAddNewAds = () => {
        const newAds: tAds = {
            id: uuidv1(),
            name: `Quảng cáo ${ads.length + 1}`,
            quantity: 0,
        };

        setAds([...ads, newAds]);
        handleSetSubCampaigns([...ads, newAds]);
    };

    const handleDeleteAds = (adsIndex: string) => {
        const newAds = ads.filter((ads: tAds) => ads.id !== adsIndex);
        setAds(newAds);

        handleSetSubCampaigns(newAds);
    };

    const handleChangeAdsQuantity = (
        index: string,
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        const newAds = ads.map((ad: tAds) =>
            ad.id === index
                ? { ...ad, quantity: Number(event.target.value) }
                : ad
        );

        setAds(newAds);

        handleSetSubCampaigns(newAds);
    };

    const handleChangeAdsName = (
        index: string,
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        const newAds = ads.map((ad: tAds) =>
            ad.id === index ? { ...ad, name: event.target.value } : ad
        );

        setAds(newAds);

        handleSetSubCampaigns(newAds);
    };

    const isValidName = (adId: string) => {
        return submitAttempted && !!adId;
    };

    const isValidQuantity = (quantity: number) => {
        return submitAttempted && quantity > 0;
    };

    return (
        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>
                            <FormControlLabel
                                label=''
                                control={
                                    <Checkbox
                                        onChange={(event) =>
                                            handleSelectAllAds(event)
                                        }
                                        indeterminate={
                                            selectedAdsIndex.length > 0 &&
                                            selectedAdsIndex.length < ads.length
                                        }
                                        checked={
                                            selectedAdsIndex.length ===
                                            ads.length
                                        }
                                        disabled={ads.length == 0}
                                    />
                                }
                            />
                        </TableCell>

                        <TableCell align='right'>
                            {selectedAdsIndex.length > 0 ? (
                                <IconButton
                                    onClick={() => handleDeleteSelectedAds()}>
                                    <DeleteOutline />
                                </IconButton>
                            ) : (
                                'Tên quảng cáo*'
                            )}
                        </TableCell>

                        <TableCell align='right'>
                            {selectedAdsIndex.length > 0 ? '' : 'Số lượng*'}
                        </TableCell>

                        <TableCell align='right'>
                            <Button
                                style={{
                                    padding: '5px 15px',
                                    border: '1px solid rgb(33, 150, 243)',
                                }}
                                onClick={() => handleAddNewAds()}>
                                + Thêm
                            </Button>
                        </TableCell>
                    </TableRow>
                </TableHead>

                <TableBody>
                    {ads?.map((ad: tAds) => {
                        return (
                            <TableRow
                                key={ad.id}
                                sx={{
                                    '&:last-child td, &:last-child th': {
                                        border: 0,
                                    },
                                }}>
                                <TableCell component='th' scope='row'>
                                    <FormControlLabel
                                        label=''
                                        control={
                                            <Checkbox
                                                name='camp'
                                                onChange={(event) =>
                                                    handleSelectAds(
                                                        event,
                                                        ad.id
                                                    )
                                                }
                                                value={ad.id}
                                                checked={selectedAdsIndex.includes(
                                                    ad.id
                                                )}
                                            />
                                        }
                                    />
                                </TableCell>

                                <TableCell>
                                    <TextField
                                        name='adsName'
                                        required
                                        error={!isValidName(ad.name)}
                                        variant='standard'
                                        fullWidth
                                        value={ad.name}
                                        helperText={
                                            !isValidName(ad.name)
                                                ? 'Hãy nhập tên của quảng cáo'
                                                : ''
                                        }
                                        onChange={(
                                            event: React.ChangeEvent<HTMLInputElement>
                                        ) => handleChangeAdsName(ad.id, event)}
                                    />
                                </TableCell>

                                <TableCell>
                                    <TextField
                                        name='adsQuantity'
                                        inputProps={{ min: 0 }}
                                        type='number'
                                        required
                                        error={!isValidQuantity(ad.quantity)}
                                        helperText={
                                            !isValidQuantity(ad.quantity)
                                                ? 'Hãy nhập số quảng cáo lớn hơn 0'
                                                : ''
                                        }
                                        variant='standard'
                                        fullWidth
                                        onChange={(
                                            event: React.ChangeEvent<HTMLInputElement>
                                        ) =>
                                            handleChangeAdsQuantity(
                                                ad.id,
                                                event
                                            )
                                        }
                                        value={ad.quantity}
                                    />
                                </TableCell>

                                <TableCell align='right'>
                                    <IconButton
                                        onClick={() => handleDeleteAds(ad.id)}
                                        disabled={selectedAdsIndex.length > 0}>
                                        <DeleteOutline />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default Ads;
