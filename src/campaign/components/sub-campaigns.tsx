import {
    Box,
    Checkbox,
    FormControlLabel,
    Grid,
    IconButton,
    Stack,
    TextField,
    ToggleButton,
    Typography,
} from '@mui/material';
import { CAMPAIGN_STATUS, tSubCampaign } from '../../types/campaign';
import { AddCircle, CheckCircle } from '@mui/icons-material';
import { useState } from 'react';
import { v1 as uuidv1 } from 'uuid';
import styled from '@emotion/styled';
import Ads from './ads';

interface SubCampaignsProps {
    subCampaigns: tSubCampaign[];
    setSubCampaigns: (data: tSubCampaign[]) => void;
    submitAttempted: boolean;
}

const SubCampaigns = ({
    subCampaigns,
    setSubCampaigns,
    submitAttempted,
}: SubCampaignsProps) => {
    const [selectedSubCampaignId, setSelectedSubCampaignId] = useState('');

    const handleSelectedSubCampaign = (newSelectedSubCampaignId: string) => {
        setSelectedSubCampaignId(newSelectedSubCampaignId);
    };

    const handleAddNewSubCampaign = () => {
        const newSubCampaign: tSubCampaign = {
            id: uuidv1(),
            name: `Chiến dịch con ${subCampaigns.length + 1}`,
            status: CAMPAIGN_STATUS.active,
            ads: [
                {
                    id: uuidv1(),
                    name: 'Quảng cáo 1',
                    quantity: 1,
                },
            ],
        };
        const newSubCampaigns = [...subCampaigns, newSubCampaign];
        setSubCampaigns(newSubCampaigns);
    };

    const selectedSubCampaign = subCampaigns.find(
        (subCampaign) => subCampaign.id === selectedSubCampaignId
    );

    const handleChangeSubCampaign = (
        field: 'status' | 'name',
        value: string | boolean
    ) => {
        const oldSubCampaigns = [...subCampaigns];
        const newSubCampaigns: tSubCampaign[] = oldSubCampaigns.map(
            (campaign: tSubCampaign) => {
                if (campaign.id === selectedSubCampaignId) {
                    return { ...campaign, [field]: value };
                }
                return campaign;
            }
        );

        setSubCampaigns(newSubCampaigns);
    };

    const isValidSubCampaign = (subCampaign: tSubCampaign) => {
        if (submitAttempted) {
            return (
                subCampaign.name !== '' &&
                subCampaign.ads.length > 0 &&
                !subCampaign.ads.some((ad) => ad.quantity <= 0) &&
                !subCampaign.ads.some((ad) => ad.name == '')
            );
        }

        return true;
    };

    const isValidSubCampaignName = (subCampaign: tSubCampaign) => {
        return submitAttempted && subCampaign.name !== '';
    };

    return (
        <Grid container>
            <SubCampaignListWrapper
                item
                xs={12}
                flexDirection='column'
                overflow='auto'>
                <IconButtonCustom onClick={handleAddNewSubCampaign}>
                    <AddCircle sx={{ fontSize: 40 }} />
                </IconButtonCustom>

                {subCampaigns.map((subCampaign) => (
                    <ToggleButtonCustom
                        selected={selectedSubCampaignId === subCampaign.id}
                        value={subCampaign.id}
                        key={subCampaign.id}
                        onClick={() =>
                            handleSelectedSubCampaign(subCampaign.id)
                        }>
                        <SubCampaign
                            subCampaign={subCampaign}
                            isValid={isValidSubCampaign(subCampaign)}
                        />
                    </ToggleButtonCustom>
                ))}
            </SubCampaignListWrapper>

            <Grid item xs={12}>
                <Box sx={{ margin: '40px 0' }}>
                    {selectedSubCampaign && (
                        <Grid container>
                            <Grid item xs={8}>
                                <TextField
                                    fullWidth
                                    name='subCampaignName'
                                    label='Tên chiến dịch con'
                                    error={
                                        !isValidSubCampaignName(
                                            selectedSubCampaign
                                        )
                                    }
                                    helperText={
                                        !isValidSubCampaignName(
                                            selectedSubCampaign
                                        )
                                            ? 'Vui lòng nhập tên chiến dịch'
                                            : ''
                                    }
                                    required
                                    value={selectedSubCampaign.name}
                                    onChange={(e) =>
                                        handleChangeSubCampaign(
                                            'name',
                                            e.target.value
                                        )
                                    }
                                />
                            </Grid>

                            <Grid item xs={4}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={
                                                selectedSubCampaign?.status ===
                                                CAMPAIGN_STATUS.active
                                            }
                                            onChange={(e) =>
                                                handleChangeSubCampaign(
                                                    'status',
                                                    e.target.checked
                                                )
                                            }
                                        />
                                    }
                                    label='Đang hoạt động'
                                />
                            </Grid>
                        </Grid>
                    )}
                </Box>
                {selectedSubCampaign && (
                    <Box>
                        <Typography variant='h5' textAlign='start'>
                            Danh sách quảng cáo
                        </Typography>

                        <Ads
                            submitAttempted={submitAttempted}
                            subCampaign={selectedSubCampaign}
                            subCampaigns={subCampaigns}
                            setSubCampaigns={setSubCampaigns}
                        />
                    </Box>
                )}
            </Grid>
        </Grid>
    );
};

const SubCampaignListWrapper = styled(Grid)`
    display: flex;
    flex-direction: row;
    width: 100%;
    gap: 24px;
    padding: 16px;
`;

const ToggleButtonCustom = styled(ToggleButton)`
    &.Mui-selected {
        border: 2px solid rgb(33, 150, 243);
    }
`;

const IconButtonCustom = styled(IconButton)`
    height: 56px;
    background-color: white;
`;

const SubCampaign = ({
    subCampaign,
    isValid,
}: {
    subCampaign: tSubCampaign;
    isValid: boolean;
}) => {
    const Wrapper = styled(Box)`
        width: 240px;
        height: 120px;
        display: flex;
        flex-direction: column;
        justify-content: center;
    `;

    const CampaignName = styled(Typography)`
        font-size: 1.25rem;
        font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
        font-weight: 500;
        line-height: 1.6;
        letter-spacing: 0.0075em;
        word-break: break-all;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2; /* start showing ellipsis when 3rd line is reached */
        white-space: pre-wrap;
        overflow: hidden;
    `;

    const AdsQuantity = styled(Typography)`
        font-size: 1.5rem;
        font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
        font-weight: 500;
        line-height: 1.6;
        letter-spacing: 0.0075em;
    `;

    const adsQuantity = subCampaign.ads
        .map((ad) => ad.quantity)
        .reduce((accumulator, current) => accumulator + current, 0);

    const isSubCampaignActive = subCampaign.status === CAMPAIGN_STATUS.active;

    return (
        <Wrapper>
            <Stack
                direction='row'
                spacing={1}
                justifyContent='space-around'
                top={12}>
                <CampaignName
                    sx={{
                        color: isValid ? 'black' : 'red',
                    }}>
                    {subCampaign.name}
                </CampaignName>
                <CheckCircle
                    sx={{ color: isSubCampaignActive ? 'green' : 'red' }}
                />
            </Stack>

            <AdsQuantity>{adsQuantity}</AdsQuantity>
        </Wrapper>
    );
};

export default SubCampaigns;
