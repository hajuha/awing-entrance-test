import {
  FormControl,
  FormHelperText,
  Input,
  InputLabel,
  Stack,
} from "@mui/material";
import { tInformation } from "../../types/campaign";
import { useMemo } from "react";

interface InforProps {
  information: tInformation;
  submitAttempted?: boolean;
  onChangeCampaignInfo: (field: string, value: string) => void;
}

const Infor = (props: InforProps) => {
  const { information, submitAttempted, onChangeCampaignInfo } = props;

  const isValidName = useMemo(() => {
    if (!submitAttempted) return true;
    console.log(!!information.name, information.name);
    return !!information.name;
  }, [submitAttempted, information.name]);

  return (
    <Stack gap={4}>
      <FormControl required>
        <InputLabel htmlFor="name">Tên chiến dịch</InputLabel>

        <Input
          id="name"
          value={information.name}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            onChangeCampaignInfo("name", event.target.value)
          }
        />
        {!isValidName && (
          <FormHelperText id="name" error>
            Vui lòng nhập tên chiến dịch
          </FormHelperText>
        )}
      </FormControl>

      <FormControl>
        <InputLabel htmlFor="description">Mô tả chiến dịch</InputLabel>

        <Input
          id="describe"
          value={information.describe}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            onChangeCampaignInfo("describe", event.target.value)
          }
        />
      </FormControl>
    </Stack>
  );
};
export default Infor;
