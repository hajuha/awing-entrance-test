import React from 'react';
import ReactDOM from 'react-dom/client';
import Campaign from './campaign/index.tsx';
import ErrorPage from './error-page.tsx';
import './index.css';
import {
    Navigate,
    RouterProvider,
    createBrowserRouter,
} from 'react-router-dom';
const router = createBrowserRouter([
    {
        path: '/',
        element: <Navigate to='/campaign' replace />,
        errorElement: <ErrorPage />,
    },
    {
        path: '/campaign',
        element: <Campaign />,
    },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
);
